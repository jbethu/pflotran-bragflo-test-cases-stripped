THIS REPOSITORY CONTAINS SEVERAL SINGLE-CELL AND MULTI-CELL TESTS THAT RUN
BRAGFLO SIMULATIONS AND PFLOTRAN SIMULATIONS. THE TESTS ARE ORGNAIZED INTO
SEVERAL SUB-DIRECTORIES WHICH CONTAIN THE INPUT DECKS AND PLOTTING SCRIPTS.

THE SCRIPT run_test_cases.py LOCATED IN THE TOP-LEVEL DIRECTORY WILL RUN THE
TESTS AND CREATE PLOTS OF THE RESULTS AUTOMATICALLY. TO USE THIS SCRIPT,
PLEASE FOLLOW THE DIRECTIONS BELOW:

===============================================================================
===============================================================================
=========== HOW TO RUN THE TEST CASES USING THE AUTOMATED SCRIPT ==============
===============================================================================
===============================================================================


1. SET UP YOUR ENVIRONMENTAL VARIABLES IN THIS WAY:

   export PETSC_DIR=/home/software/petsc        ! this must point to your 
                                                ! top-level petsc directory
   export PETSC_ARCH=linux-gcc-5_4_0-debug      ! this will be specific to your 
                                                ! architecture
   export PFLOTRAN_DIR=/home/software/pflotran  ! this must point to your 
                                                ! top-level pflotran directory
   export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran

   export BRAGFLO_DIR=/home/software/bragflo      ! this must point to your 
                                                  ! top-level bragflo directory
   export BINREAD_DIR=$BRAGFLO_DIR/BRAGFLO/Source ! this must point to where 
                                                  ! binread.py is located


2. RUN THE SCRIPT

  > python run_test_cases.py -flag

   POSSIBLE FLAGS INCLUDE:  -single  -multi  -all  -help  (CHOOSE ONE ONLY)

   THIS SCRIPT WILL RUN THE CHOSEN TESTS WITH BRAGFLO AND PFLOTRAN, CONVERT
   THE BRAGFLO OUTPUT INTO HDF5 FORMAT, AND PLOT THE RESULTS.


3. IF YOU ONLY WANT TO RUN A FEW TESTS, THEN COMMENT OUT THE TESTS YOU DO
   NOT WANT TO RUN WITHIN THE run_test_cases.py SCRIPT. IF THESE HAPPEN TO BE
   SINGLE CELL TESTS, REMEMBER TO ALSO COMMENT OUT THOSE TESTS IN THE PLOTTING
   SCRIPT plot_pf_bf.py LOCATED IN /tests_single_cell.



===============================================================================
===============================================================================
=========== HOW TO ADD A NEW TEST TO THE AUTOMATED SCRIPT =====================
===============================================================================
===============================================================================

1. ADD A NEW DIRECTORY UNDER tests_multi_cell OR tests_single_cell. THE NEW
   DIRECTORY SHOULD CONTAIN:

   BRAGFLO INPUT DECK(s) NAMED bf_testname.inp
   PFLOTRAN INPUT DECK(s) NAMED pf_testname.in
   FOR PLOTTING:
      - A PYTHON SCRIPT FOR PLOTTING THE BRAGFLO AND PFLOTRAN RESULTS MUST BE
        PROVIDED FOR EACH NEW TEST IF IT IS A MULTI-CELL TEST.
      - ADDITIONAL CODE MUST BE ADDED TO THE EXISTING PYTHON SCRIPT IF IT A
        SINGLE-CELL TEST (/tests_single_cell/plot_pf_bf.py).
   README FILES TO RUN THE TEST(s) INDIVIDUALLY, FOR BOTH RUNNING THE TEST(s) 
      AND POST-RUNNING TO PLOT THE RESULTS WOULD BE NICE.

2. ADD YOUR NEW TEST DIRECTORY TO THE run_test_cases.py SCRIPT. THINGS TO WATCH
   OUT FOR:

   FOR SINGLE-CELL TESTS:
      - ADD YOUR TEST SUB-DIRECTORY NAME TO THE single_cell_tests LIST:
        single_cell_tests[0].append("YOUR_NEW_TESTNAME")
      - EXTEND YOUR NEW TEST'S LIST WITH A TUPLE OF INFORMATION THAT INDICATES:
        single_cell_tests[#].append(("YOUR_NEW_TESTNAME", \
                                     "-csd BF_CLOSURE_FILENAME.dat"))
   FOR MULTI-CELL TESTS:
      - ADD YOUR TEST SUB-DIRECTORY NAME TO THE multi_cell_tests LIST:
        multi_cell_tests[0].append("YOUR_NEW_TESTNAME")
      - EXTEND YOUR NEW TEST'S LIST WITH A TUPLE OF INFORMATION THAT INDICATES:
        multi_cell_tests[#].append(("YOUR_NEW_TESTNAME", \
                                     "-csd BF_CLOSURE_FILENAME.dat", \
                                     "PYTHON_PLOT_SCRIPT_NAME.py", \
                                     "PLOT POINTS"))
         

