python ../../tests_single_cell/binread.py --plane xy --postarray pcgw --binary bf_6a_allgen.bin --hdf5 bf_6a_allgen.h5 --unitgrid true --snapshot_timeunit y --round_time true
python ../../tests_single_cell/binread.py --plane xy --postarray pcgw --binary bf_6a_allgen_nomaxd.bin --hdf5 bf_6a_allgen_nomaxd.h5 --unitgrid true --snapshot_timeunit y --round_time true
