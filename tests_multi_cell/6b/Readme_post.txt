#on jt
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6b.bin --ascii bf_6b.txt --hdf5 bf_6b.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6b_wgas.bin --ascii bf_6b_wgas.txt --hdf5 bf_6b_wgas.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6b_wgasnocreep.bin --ascii bf_6b_wgasnocreep.txt --hdf5 bf_6b_wgasnocreep.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6b_winj.bin --ascii bf_6b_winj.txt --hdf5 bf_6b_winj.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6b_wgas_ttol.bin --ascii bf_6b_wgas_ttol.txt --hdf5 bf_6b_wgas_ttol.h5

python bfpf_compres.py bf_6b pf_6b 3 2
python bfpf_compres.py bf_6b_wgas pf_6b_wgas 3 2
python bfpf_compres.py bf_6b_wgasnocreep pf_6b_wgasnocreep 3 2
python bfpf_compres.py bf_6b_winj pf_6b_winj 3 2
python bfpf_compres.py bf_6b_wgas_ttol pf_6b_wgas_ttol 3 2

