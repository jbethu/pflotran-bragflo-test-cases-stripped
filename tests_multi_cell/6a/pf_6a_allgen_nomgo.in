!==============================================================================
! BRAGFLO/PFLOTRAN Comparison Case 6a - 2-D cross section with gas generation over full domain (no mgo)
!==============================================================================

!=========================== SIMULATION MOD ===================================
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW FLOW
      MODE WIPP_FLOW
      OPTIONS
        FIX_UPWIND_DIRECTION
        MAX_PRESSURE_CHANGE 1.d20
        MAX_SATURATION_CHANGE 1.d20
        GAS_COMPONENT_FORMULA_WEIGHT 2.01588D0 ! H2 kg/kmol
        LIQUID_EQUATION_TOLERANCE 1.d-6
        GAS_EQUATION_TOLERANCE 1.d-6
        LIQUID_PRESSURE_TOLERANCE 1.d-5
        GAS_SATURATION_TOLERANCE 1.d-4
      END
    END
  END
END

SUBSURFACE

!=========================== REGRESSION =======================================
REGRESSION
  CELLS
    3
    8
    13
  /
END

!=========================== FLUID PROPERTIES =================================
FLUID_PROPERTY
  PHASE LIQUID
  DIFFUSION_COEFFICIENT 0.d0
END

FLUID_PROPERTY
  PHASE GAS
  DIFFUSION_COEFFICIENT 0d0
END

!=========================== FLUID CONDITIONS =================================
EOS WATER
  DENSITY exponential 1.2200000d+03 1.0132500d+05 3.1000000d-10
  VISCOSITY constant 2.1000000d-03
  ENTHALPY constant 1.8990000d+06
END

EOS GAS
  DENSITY RKS
    HYDROGEN
    USE_EFFECTIVE_PROPERTIES
    USE_CUBIC_ROOT_SOLUTION
    TC 4.36000E+01
    PC 2.04700E+06
    AC 0.00000E+00
    A 4.27470E-01
    B 8.66400E-02
  END
  VISCOSITY CONSTANT 8.9338900d-06
  HENRYS_CONSTANT CONSTANT 1.0000000d+10
END

REFERENCE_PRESSURE 101325.d0
REFERENCE_TEMPERATURE 27.d0 ! 300.15 K

!=========================== GRID =============================================
GRID
  TYPE STRUCTURED
  NXYZ 11 1 21      
  DXYZ
    1@0.5d0 10@1.d0 # [m]
    1.d0 # [m]
    1.d0 # [m]
  END
  GRAVITY -0.000000 0.000000 -9.806650
END

!=========================== REGIONS ==========================================
REGION ALL
  COORDINATES
    0.d0 0.d0 0.d0
    10.5d0 1.d0 21.d0
  END
END

REGION WP1
  COORDINATES
    0.d0 0.d0  10.d0
    0.5d0 1.d0 11.d0
  END
END

REGION 1
  BLOCK 1 11 1 1 1 1
END

REGION 2
  BLOCK 1 11 1 1 2 2
END

REGION 3
  BLOCK 1 11 1 1 3 3
END

REGION 4
  BLOCK 1 11 1 1 4 4
END

REGION 5
  BLOCK 1 11 1 1 5 5
END

REGION 6
  BLOCK 1 11 1 1 6 6
END

REGION 7
  BLOCK 1 11 1 1 7 7
END

REGION 8
  BLOCK 1 11 1 1 8 8
END

REGION 9
  BLOCK 1 11 1 1 9 9
END

REGION 10
  BLOCK 1 11 1 1 10 10
END

REGION 11
  BLOCK 1 11 1 1 11 11
END

REGION 12
  BLOCK 1 11 1 1 12 12
END

REGION 13
  BLOCK 1 11 1 1 13 13
END

REGION 14
  BLOCK 1 11 1 1 14 14
END

REGION 15
  BLOCK 1 11 1 1 15 15
END

REGION 16
  BLOCK 1 11 1 1 16 16
END

REGION 17
  BLOCK 1 11 1 1 17 17
END

REGION 18
  BLOCK 1 11 1 1 18 18
END

REGION 19
  BLOCK 1 11 1 1 19 19
END

REGION 20
  BLOCK 1 11 1 1 20 20
END

REGION 21
  BLOCK 1 11 1 1 21 21
END

!=========================== MATERIAL PROPERTIES ==============================

MATERIAL_PROPERTY SOIL
  ID 1
  POROSITY 0.45
  TORTUOSITY 1.0000000d+00
  ROCK_DENSITY 2.6500000d+03
  HEAT_CAPACITY 830.
  THERMAL_CONDUCTIVITY_WET 2.
  THERMAL_CONDUCTIVITY_DRY 0.5
  SOIL_COMPRESSIBILITY_FUNCTION BRAGFLO
  BULK_COMPRESSIBILITY 4.50d-9
  SOIL_REFERENCE_PRESSURE INITIAL_PRESSURE
  PERMEABILITY
    PERM_X_LOG10 -17.0
    PERM_Y_LOG10 -17.0
    PERM_Z_LOG10 -17.0
  END
  CHARACTERISTIC_CURVES SOIL_cc
END

CHARACTERISTIC_CURVES SOIL_cc
  SATURATION_FUNCTION BRAGFLO_KRP4
    KPC 2
    LAMBDA 7.0000000d-01
    PCT_A 8.300000d+04
    PCT_EXP 0.000000d+00
    LIQUID_RESIDUAL_SATURATION 2.0000000d-01
    GAS_RESIDUAL_SATURATION 1.d-05
    MAX_CAPILLARY_PRESSURE 1.0000000d+08
  END
  PERMEABILITY_FUNCTION BRAGFLO_KRP4_LIQ
    PHASE LIQUID
    LAMBDA 7.0000000d-01
    LIQUID_RESIDUAL_SATURATION 2.0000000d-01
    GAS_RESIDUAL_SATURATION 1.d-05
  END
  PERMEABILITY_FUNCTION BRAGFLO_KRP4_GAS
    PHASE GAS
    LAMBDA 0.7d0
    LIQUID_RESIDUAL_SATURATION 2.0000000d-01
    GAS_RESIDUAL_SATURATION 1.d-05
  END
END

!=========================== SOLVER OPTIONS ===================================
NEWTON_SOLVER FLOW
  ATOL 1.0000000d-8
  STOL 1.0000000d-8
  RTOL 1.0000000d-8
  MAXIT 20
END

LINEAR_SOLVER FLOW
  SOLVER DIRECT
END

!=========================== OUTPUT OPTIONS ===================================
OUTPUT
  TIMES y 0.0 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0 100.0 350.0 1000.0 3000.0 5000.0 7000.0 9000.0 10000.0
  FORMAT HDF5
  VARIABLES
    LIQUID_PRESSURE
    GAS_PRESSURE
    EFFECTIVE_POROSITY
    LIQUID_SATURATION
    GAS_SATURATION
    CAPILLARY_PRESSURE
    LIQUID_DENSITY
    GAS_DENSITY
  END
END

!=========================== TIMES ============================================
TIME
  FINAL_TIME 10000.0d0 y
  INITIAL_TIMESTEP_SIZE 3.1709800d-08 y
  MAXIMUM_TIMESTEP_SIZE 55.d0 y at 0.0d0 y
END

!=========================== FLOW CONDITIONS ==================================
FLOW_CONDITION 1
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.08320d+06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 2
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.07340d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 3
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.06360d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 4
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.05380d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 5
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.04400d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 6
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.03420d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 7
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.02440d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 8
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.01470d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 9
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 4.00490d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 10
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.99510d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 11
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.98530d06
  LIQUID_SATURATION 9.999900d-01
END

FLOW_CONDITION 12
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.97550d06
  LIQUID_SATURATION 9.999800d-01
END

FLOW_CONDITION 13
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.96570d06
  LIQUID_SATURATION 9.972900d-01
END

FLOW_CONDITION 14
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.95590d06
  LIQUID_SATURATION 9.919200d-01
END

FLOW_CONDITION 15
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.94610d06
  LIQUID_SATURATION 9.866300d-01
END

FLOW_CONDITION 16
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.93630d06
  LIQUID_SATURATION 9.814300d-01
END

FLOW_CONDITION 17
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.92650d06
  LIQUID_SATURATION 9.763100d-01
END

FLOW_CONDITION 18
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.91670d06
  LIQUID_SATURATION 9.712700d-01
END

FLOW_CONDITION 19
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.90690d06
  LIQUID_SATURATION 9.663100d-01
END

FLOW_CONDITION 20
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.89710d06
  LIQUID_SATURATION 9.614300d-01
END

FLOW_CONDITION 21
  TYPE
    LIQUID_PRESSURE dirichlet
    LIQUID_SATURATION dirichlet
  END
  LIQUID_PRESSURE 3.88730d06
  LIQUID_SATURATION 9.566200d-01
END

!=========================== INITIAL AND BOUNDARY CONDITIONS ==================
INITIAL_CONDITION 1
  FLOW_CONDITION 1
  REGION 1
END

INITIAL_CONDITION 2
  FLOW_CONDITION 2
  REGION 2
END

INITIAL_CONDITION 3
  FLOW_CONDITION 3
  REGION 3
END

INITIAL_CONDITION 4
  FLOW_CONDITION 4
  REGION 4
END

INITIAL_CONDITION 5
  FLOW_CONDITION 5
  REGION 5
END

INITIAL_CONDITION 6
  FLOW_CONDITION 6
  REGION 6
END

INITIAL_CONDITION 7
  FLOW_CONDITION 7
  REGION 7
END

INITIAL_CONDITION 8
  FLOW_CONDITION 8
  REGION 8
END

INITIAL_CONDITION 9
  FLOW_CONDITION 9
  REGION 9
END

INITIAL_CONDITION 10
  FLOW_CONDITION 10
  REGION 10
END

INITIAL_CONDITION 11
  FLOW_CONDITION 11
  REGION 11
END

INITIAL_CONDITION 12
  FLOW_CONDITION 12
  REGION 12
END

INITIAL_CONDITION 13
  FLOW_CONDITION 13
  REGION 13
END

INITIAL_CONDITION 14
  FLOW_CONDITION 14
  REGION 14
END

INITIAL_CONDITION 15
  FLOW_CONDITION 15
  REGION 15
END

INITIAL_CONDITION 16
  FLOW_CONDITION 16
  REGION 16
END

INITIAL_CONDITION 17
  FLOW_CONDITION 17
  REGION 17
END

INITIAL_CONDITION 18
  FLOW_CONDITION 18
  REGION 18
END

INITIAL_CONDITION 19
  FLOW_CONDITION 19
  REGION 19
END

INITIAL_CONDITION 20
  FLOW_CONDITION 20
  REGION 20
END

INITIAL_CONDITION 21
  FLOW_CONDITION 21
  REGION 21
END

!=========================== STRATA ===========================================
STRATA
  MATERIAL SOIL
  REGION ALL
END

END_SUBSURFACE

!=========================== GAS GENERATION AND WATER BALANCE =================

WIPP_SOURCE_SINK
  BRUCITEC  5.40958610500549d-08  ![mol-MgOH2/kg-MgO/s] MgO inundated hydration rate in Salado brine
  BRUCITEH  1.90935050526199d-08  ![mol-MgOH2/kg-MgO/s] MgO humid hydration rate
  HYMAGCON  6.47595498826265d-10  ![mol-hydromag/kg-hydromag/s] hydromagnesite to magnesite conversion rate
  SAT_WICK  0.322252637147903d0   ![-] wicking saturation parameter
  SALT_PERCENT  3.2400d1          ![100*kg salt/kg water] weight percent salt in brine (rxns produce brine, not just water)
  GRATMICI  2.38570594086619d-10  ![mol-cell/kg-cell/s] inundated biodegradation rate for cellulose
  GRATMICH  3.38837738770187d-11  ![mol-cell/kg-cell/s] humid biodegradation rate for cellulose
  CORRMCO2  6.67748215472072d-15  ![m/s] inundated steel corrosion rate without microbial gas generation
  HUMCORR   0.d0    ![m/s] humid steel corrosion rate
  ASDRUM    6.d0    ![m2] surface area of corrodable metal per drum
  ALPHARXN -1.d3    ![-]
  SOCMIN    1.5d-2  ![-]
  BIOGENFC  0.725563609600067   ![-]
  PROBDEG   1       ![-]

  STOICHIOMETRIC_MATRIX
  # hydro  H2     H2O       Fe      Cell   FeOH2  FeS    MgO    MgOH2  MgCO3 
    0.0d0  1.0d0 -2.0d0     -1.0d0  0.0d0  1.0d0  0.0d0  0.0d0  0.0d0  0.0d0 # anoxic iron corrosion reaction
    0.0d0  0.0d0  0.91293d0  0.0d0 -1.0d0  0.0d0  0.0d0  0.0d0  0.0d0  0.0d0 # microbial gas generation reaction
    0.0d0 -1.0d0  2.0d0      0.0d0  0.0d0 -1.0d0  1.0d0  0.0d0  0.0d0  0.0d0 # iron hydroxide sulfidation
    0.0d0  0.0d0  0.0d0     -1.0d0  0.0d0  0.0d0  1.0d0  0.0d0  0.0d0  0.0d0 # metallic iron sulfidation
    0.0d0  0.0d0 -1.0d0      0.0d0  0.0d0  0.0d0  0.0d0 -1.0d0  1.0d0  0.0d0 # MgO hydration
    0.25d0 0.0d0  0.0d0      0.0d0  0.0d0  0.0d0  0.0d0  0.0d0 -1.25d0 0.0d0 # Mg(OH)2 (brucite) carbonation
    0.0d0  0.0d0  0.0d0      0.0d0  0.0d0  0.0d0  0.0d0 -1.0d0  0.0d0  1.0d0 # MgO carbonation
   -1.0d0  0.0d0  4.0d0      0.0d0  0.0d0  0.0d0  0.0d0  0.0d0  1.0d0  4.0d0 # hydromagnesite conversion
  END 
  # note: multiple inventories may be included, but here there is only one
  INVENTORY INV1 #each inventory is specific to region it is going in, so SCALE_BY_VOLUME when using whole repo inventory
    VREPOS     438406.08 m^3 ! optional - only needed if a WASTE_PANEL including this inventory needs to SCALE_BY_VOLUME
    SOLIDS #total kg in repository
      IRONCHW  1.09d7 kg   ! mass of Fe-based material in CH waste
      IRONRHW  1.35d6 kg   ! mass of Fe-based material in RH waste
      IRNCCHW  3.00d7 kg   ! mass of Fe containers for CH waste
      IRNCRHW  6.86d6 kg   ! mass of Fe containers for RH waste
      CELLCHW  3.55d6 kg   ! mass of cellulosics in CH waste
      CELLRHW  1.18d5 kg   ! mass of cellulosics in RH waste
      CELCCHW  7.23d5 kg   ! mass of cellulosics in container materials for CH waste
      CELCRHW  0.d0   kg   ! mass of cellulosics in container materials for RH waste
      CELECHW  2.60d5 kg   ! mass of cellulosics in emplacement materials for CH waste
      CELERHW  0.d0   kg   ! mass of cellulosics in emplacement materials for RH waste
      RUBBCHW  1.09d6 kg   ! mass of rubber in CH waste
      RUBBRHW  8.80d4 kg   ! mass of rubber in RH waste
      RUBCCHW  6.91d4 kg   ! mass of rubber in container materials for CH waste
      RUBCRHW  4.18d3 kg   ! mass of rubber in container materials for RH waste
      RUBECHW  0.d0   kg   ! mass of rubber in emplacement materials for CH waste
      RUBERHW  0.d0   kg   ! mass of rubber in emplacement materials for RH waste
      PLASCHW  5.20d6 kg   ! mass of plastics in CH waste
      PLASRHW  2.93d5 kg   ! mass of plastics in RH waste
      PLSCCHW  2.47d6 kg   ! mass of plastics in container materials for CH waste
      PLSCRHW  3.01d5 kg   ! mass of plastics in container materials for RH waste
      PLSECHW  1.25d6 kg   ! mass of plastics in emplacement materials for CH waste
      PLSERHW  0.d0   kg   ! mass of plastics in emplacement materials for RH waste
      PLASFAC  1.7d0       ! mass ratio of plastics to equivalent carbon
      MGO_EF   0.0d0       ! MgO excess factor: ratio mol-MgO/mol-Organic-C
      DRMCONC  1.8669852   ! [-/m3] number of metal drums per m3 in a panel in ideal packing (DRROOM/VROOM = 6804/3644.378))
    END
    AQUEOUS
      NITRATE 2.74d7   ! moles in panel  QINIT[B:32]
      SULFATE 4.91d6   ! moles in panel  QINIT[B:31]
    END
  END

  WASTE_PANEL ALL
    REGION ALL
    INVENTORY INV1
    SCALE_BY_VOLUME YES
END

END_WIPP_SOURCE_SINK
