#!/bin/bash

# Set environmental variables
export PETSC_DIR=$HOME/dev/petsc
export PETSC_ARCH=linux-gcc-5_4_0-debug
export PFLOTRAN_DIR=$HOME/dev/rssarathi/pflotran
export PFLOTRAN_BUILD=$PFLOTRAN_DIR/build/$PETSC_ARCH
export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran

export TEST_DIR=/home/rsarath/dev/bragflo/TEST
export BRAGFLO_DIR=/home/rsarath/dev/bragflo/BRAGFLO/Source/
export BINREAD_DIR=$BRAGFLO_DIR

# on jt
export PETSC_DIR=/utilities/petsc
export PETSC_ARCH=arch-linux2-opt
export PFLOTRAN_DIR=/utilities/pflotran/pflotran-temp
export PFLOTRAN_BUILD=$PFLOTRAN_DIR/build/$PETSC_ARCH
export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran

# on santana
export TEST_DIR=./
export BRAGFLO_DIR=/home/baday/Codes/SER17001/MyBuildArea/BRAGFLO/BRAGFLO
export BINREAD_DIR=./

# List of deck files to run
DECK_FILES=(
case060100_0d_gas_injection
case060200_0d_gas_generation_midsat
case060210_0d_gas_generation_hisat
case060220_0d_gas_generation_lowsat
case060230_0d_gas_generation_corbio_midsat
case060240_0d_gas_generation_corbio_hisat
case060250_0d_gas_generation_corbio_lowsat
case060300_0d_creep_static
case060310_0d_creep_gas_injection
case060320_0d_creep_gas_generation
case060400_0d_porecomp_gas_injection
case060500_0d_fracture_gas_injection
case060600_0d_klinkenberg_gas_injection
case060700_0d_rks_calc_gas_injection
case072100_reduced_repo
);
# case060610_0d_klinkenberg_fracture

# DECK=case072100_reduced_repo

# Delete output
# rm -rf case*.bfout case*.bfsum case*.bin case*.hdf5 case*.h5 case*.out case*.pnl case*.regression case*.tec


# Run all BRAGFLO 
for DECK in ${DECK_FILES[@]}; do
  echo $DECK
  # $BRAGFLO_DIR/bragflo -input $DECK.bfi -csd closure.dat -binary $DECK.bin -output $DECK.bfout -summary $DECK.bfsum -rout $DECK.rout
  python $BINREAD_DIR/binread.py --plane xz --binary $DECK.bin --hdf5 $DECK.bf.h5
done

# Run all PFLOTRAN
for DECK in ${DECK_FILES[@]}; do
  echo $DECK
  $PFLOTRAN_BUILD/pflotran -pflotranin $DECK.pfi
done


